# Alice's Adventures in Wonderland

![Alice's Adventures in Wonderland][1]

## ALICE'S ADVENTURES IN WONDERLAND

### BY LEWIS CARROLL

*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998

[A BookVirtual Digital Edition, v1.2][2]
November, 2000
![Alice's Adventures in Wonderland][3]

```md
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.

Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?

Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not more than once a minute.

Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.

And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It is next time!"
  The happy voices cry.

Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.

Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
```

## Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
---  
1

![DOWN THE RABBIT-HOLE][4]
## Chapter I  

DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is

---
6

Down, down, down. There was nothing else to  
do, so Alice soon began talking again. “Dinah ’ll  
miss me very much to-night, I should think !”  
(Dinah was the cat.) “ I hope they’ll remember  
her saucer of milk at tea-time. Dinah, my dear!  
I wish you were down here with me ! There  
are no mice in the air, I’m afraid, but you  
might catch a bat, and that’s very like a mouse,  
you know. But do cats eat bats, I wonder?”  
And here Alice began to get rather sleepy, and  
went on saying to herself, in a dreamy sort  
of way, “Do cats eat bats ? Do cats eat bats ?”  
and sometimes, “Do bats eat cats?” for, you  
see, as she couldn’t answer either question, it  
didn’t much matter which way she put it. She  
felt that she was dozing off, and had just begun  
to dream that she was walking hand in hand  
with Dinah, and was saying to her very  
earnestly, “Now, Dinah, tell me the truth: did  
you ever eat a bat ?” when suddenly, thump !  
thump! down she came upon a heap of sticks  
and dry leaves, and the fall was over.  

---
33

had paused as if it thought that somebody ought  
to speak, and no one else seemed inclined to  
say anything.  
“ Why,” said the Dodo, “ the best way to  
explain it is to do it.” (And as you might like  
to try the thing yourself, some winter day, I  
will tell you how the Dodo managed it.)  
First it marked out a race-course, in a sort  
of circle, (“ the exact shape doesn ’t matter,” it  
said,) and then all the party were placed along  
the course, here and there. There was no “ One,  
two, three, and away,” but they began running  
when they liked, and left off when they liked, so  
that it was not easy to know when the race was  
over. However, when they had been running half-  
an-hour or so, and were quite dry again, the Dodo  
suddenly called out, “ The race is over !” and they  
all crowded round it, panting, and asking, “ But  
who has won ?”  
This question the Dodo could not answer  
without a great deal of thought, and it sat for  
a long time with one finger pressed upon its  

---
69

which were the two sides of it; and, as it was  
perfectly round, she found this a very difficult  
question. However, at last she stretched her  
arms round it as far as they would go, and  
broke off a bit of the edge with each hand.  
“And now which is which?” she said to her-  
self, and nibbled a little of the right-hand bit to  
try the effect: the next moment she felt a vio-  
lent blow underneath her chin: it had struck  
her foot!  
She was a good deal frightened by this very  
sudden change, but she felt that there was no  
time to be lost, as she was shrinking rapidly;  
so she set to work at once to eat some of the  
other bit. Her chin was pressed so closely  
against her foot, that there was hardly room to  
open her mouth; but she did it at last, and  
managed to swallow a morsel of the left-hand  
bit.  

```md
   *     *     *     *     *
      *     *     *     *
   *     *     *     *     *
```

---
87

or two : wouldn’t it be murder to leave it  
behind?” She said the last words out loud, and  
the little thing grunted in reply (it had left off  
sneezing by this time). “ Don’t grunt,” said  
Alice : “ that ’s not at all a proper way of  
expressing yourself.”  
The baby grunted again, and Alice looked  
very anxiously into its face to see what was the  
matter with it. There could be no doubt that  
it had a very turn-up nose, much more like a  
snout than a real nose ; also its eyes were  
getting extremely small, for a baby : altogether  
Alice did not like the look of the thing at all,  
“ —but perhaps it was only sobbing,” she  
thought, and looked into its eyes again, to see  
if there were any tears.  
No, there were no tears. “ If you’re going to  
turn into a pig, my dear,” said Alice, seriously,  
“I’ll have nothing more to do with you. Mind  
now !” The poor little thing sobbed again, (or  
grunted, it was impossible to say which,) and  
they went on for some while in silence.

[1]: https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png
[2]: https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf
[3]: https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg
[4]: https://www.gutenberg.org/files/19778/19778-h/images/p001.png
