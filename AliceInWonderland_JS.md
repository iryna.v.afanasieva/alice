# Alice's Adventures in Wonderland

![Alice's Adventures in Wonderland][1]

## ALICE'S ADVENTURES IN WONDERLAND

### BY LEWIS CARROLL

*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998

[A BookVirtual Digital Edition, v1.2][2]
November, 2000
![Alice's Adventures in Wonderland][3]

```md
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.

Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?

Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not more than once a minute.

Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.

And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It is next time!"
  The happy voices cry.

Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.

Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
```

## Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
---  
1

![DOWN THE RABBIT-HOLE][4]
## Chapter I  

DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is

---
4

a jar from one of the shelves as she passed ; it  
was labelled “ ORANGE MARMALADE,” but  
to her great disappointment it was empty: she did  
not like to drop the jar for fear of killing some-  
body underneath, so managed to put it into  
one of the cupboards as she fell past it.  
“ Well !” thought Alice to herself, “ after  
such a fall as this, I shall think nothing of  
tumbling down stairs ! How brave they ’ll all  
think me at home ! Why, I wouldn’t say any-  
thing about it, even if I fell off the top of  
the house !” (Which was very likely true.)  
Down, down, down. Would the fall never  
come to an end ? “ I wonder how many miles  
I ’ve fallen by this time ?” she said aloud. “ I  
must be getting somewhere near the centre of  
the earth. Let me see : that would be four  
thousand miles down, I think—” (for, you see,  
Alice had learnt several things of this sort in  
her lessons in the schoolroom, and though this  
was not a very good opportunity for showing off  
her knowledge, as there was no one to listen to  

---
110

 “Really, now you ask me,” said Alice, very  
 much confused, “I don’t think——”  
 “Then you shouldn’t talk,” said the Hatter.  

 ![A MAD TEA-PARTY][5]

  This piece of rudeness was more than Alice  
 could bear: she got up in great disgust, and  
 walked off: the Dormouse fell asleep instantly,  
 and neither of the others took the least notice  
 of her going, though she looked back once or  
 twice, half hoping that they would call after  
 her: the last time she saw them, they were  
 trying to put the Dormouse into the teapot.  

---
111

 At any rate I’ll never go there again!” said  
 Alice as she picked her way through the wood.  
 “It’s the stupidest tea-party I ever was at in  
 all my life!”  
 Just as she said this, she noticed that one  
 of the trees had a door leading right into it.  
 “That’s very curious!” she thought. “But  
 everything’s curious to-day. I think I may as  
 well go in at once.” And in she went.  
 Once more she found herself in the long hall,  
 and close to the little glass table. “Now, I’ll  
 manage better this time,” she said to herself,  
 and began by taking the little golden key, and  
 unlocking the door that led into the garden.  
 Then she went to work nibbling at the mushroom  
 (she had kept a piece of it in her pocket) till  
 she was about a foot high: then she walked  
 down the little passage: and then—she found  
 herself at last in the beautiful garden, among  
 the bright flowerbeds and the cool fountains.

---
134

“It’s a mineral, I think,” said Alice.  
“ Of course it is,” said the Duchess, who  
seemed ready to agree to everything that Alice  
said; “there ’s a large mustard-mine near here.  
And the moral of that is—‘The more there is  
of mine, the less there is of yours.’ ”  
“ Oh, I know !” exclaimed Alice, who had  
not attended to this last remark, “it’s a veget-  
able. It doesn ’t look like one, but it is.”  
“I quite agree with you,” said the Duchess,  
“and the moral of that is—‘Be what you would  
seem to be’—or, if you ’d like it put more  
simply—‘ Never imagine yourself not to be  
otherwise than what it might appear to others  
that what you were or might have been was  
not otherwise than what you had been would  
have appeared to them to be otherwise.’ ”  
“ I think I should understand that better,”  
Alice said very politely, “ if I had it written  
down: but I can ’t quite follow it as you say it.”  
“That ’s nothing to what I could say if I  
chose,” the Duchess replied in a pleased tone.  

[1]: https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png
[2]: https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf
[3]: https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg
[4]: https://www.gutenberg.org/files/19778/19778-h/images/p001.png
[5]: https://www.gutenberg.org/files/19778/19778-h/images/p102.png
