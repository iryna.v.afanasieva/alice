# Alice's Adventures in Wonderland

![Alice's Adventures in Wonderland][1]

## ALICE'S ADVENTURES IN WONDERLAND

### BY LEWIS CARROLL

*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998

[A BookVirtual Digital Edition, v1.2][2]
November, 2000
![Alice's Adventures in Wonderland][3]

```md
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.

Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?

Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not more than once a minute.

Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.

And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It is next time!"
  The happy voices cry.

Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.

Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
```

## Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
---  
1

![DOWN THE RABBIT-HOLE][4]
## Chapter I  

DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is

---
24

“ I shall be punished for it now, I suppose, by  
being drowned in my own tears ! That will be  
a queer thing, to be sure ! However, everything  
is queer to-day.”  
Just then she heard something splashing  
about in the pool a little way off, and she swam  
nearer to make out what it was : at first she  
thought it must be a walrus or hippopotamus,  
but then she remembered how small she was  
now, and she soon made out that it was only  
a mouse, that had slipped in like herself.  
“ Would it be of any use, now,” thought  
Alice, “ to speak to this mouse ? Everything is  
so out-of-the-way down here, that I should think  
very likely it can talk : at any rate there ’s  
no harm in trying.” So she began : “ O Mouse,  
do you know the way out of this pool ? I am  
very tired of swimming about here, O Mouse !”  
(Alice thought this must be the right way of  
speaking to a mouse : she had never done such  
a thing before, but she remembered having seen  
in her brother’s Latin Grammar, “ A mouse—

---
37

so that her idea of the tale was something like

```md
                  “Fury said to a
               mouse, That he
            met in the
         house,
      ‘Let us
         both go to
            law: I will
               prosecute
                  YOU. —Come,
                     I’ll take no
                        denial; We
                     must have a
                  trial: For
               really this
            morning I’ve
         nothing
      to do.’
         Said the
            mouse to the
               cur, ‘Such
                  a trial,
                     dear Sir,
                        With
                     no jury
                  or judge,
               would be
            wasting
         our
            breath.’
               ‘I’ll be
                  judge, I’ll
                     be jury,’
                        Said
                  cunning
                     old Fury:
                        ‘I’ll
                     try the
                        whole
                           cause,
                              and
                        condemn
                     you
                  to
                     death.’”
```

---
71

trees under which she had been wandering,  
when a sharp hiss made her draw back in a  
hurry: a large pigeon had flown into her face,  
and was beating her violently with its wings.  
“Serpent!” screamed the Pigeon.  
“I’m not a serpent!” said Alice indignantly.  
“Let me alone !”  
“Serpent, I say again !” repeated the Pigeon,  
but in a more subdued tone, and added with  
a kind of sob, “ I’ve tried every way, and  
nothing seems to suit them !”  
“I haven ’t the least idea what you’re talk-  
ing about,” said Alice.  
“I’ve tried the roots of trees, and I’ve tried  
banks, and I’ve tried hedges,” the Pigeon went  
on, without attending to her ; “ but those  
serpents! There’s no pleasing them !”  
Alice was more and more puzzled, but she  
thought there was no use in saying anything  
more till the Pigeon had finished.  
“ As if it wasn’t trouble enough hatching  
the eggs,” said the Pigeon; “but I must be on  

---
80

the trees behind him.  
“-or next day, maybe,” the Footman con-  
tinued in the same tone, exactly as if nothing  
had happened.  
“How am I to get in?” asked Alice again in  
a louder tone.  
“Are you to get in at all ?” said the Foot-  
man. “That’s the first question, you know.”  
It was, no doubt: only Alice did not like to  
be told so. “It’s really dreadful,” she muttered  
to herself, “ the way all the creatures argue.  
It’s enough to drive one crazy !”  
The Footman seemed to think this a good  
opportunity for repeating his remark, with  
variations. “ I shall sit here,” he said, “on and  
off, for days and days.”  
“But what am I to do?” said Alice.  
“Anything you like,” said the Footman, and  
began whistling.  
“Oh, there’s no use in talking to him,” said  
Alice desperately : “he’s perfectly idiotic !” And  
she opened the door and went in.  

---
123

become of me ? They ’re dreadfully fond of  
beheading people here : the great wonder is,  
that there’s any one left alive!”  
She was looking about for some way of  
escape, and wondering whether she could get  
away without being seen, when she noticed a  
curious appearance in the air : it puzzled her  
very much at first, but after watching it a  
minute or two she made it out to be a grin,  
and she said to herself, “It’s the Cheshire Cat:  
now I shall have somebody to talk to.”  
“How are you getting on?” said the Cat,  
as soon as there was mouth enough for it to  
speak with.  
Alice waited till the eyes appeared, and then  
nodded. “It ’s no use speaking to it,” she  
thought, “till its ears have come, or at least  
one of them.” In another minute the whole  
head appeared, and then Alice put down her  
flamingo, and began an account of the game,  
feeling very glad she had some one to listen to  
her. The Cat seemed to think that there was  

---
172

“ or I ’ll have you executed.”  
The miserable Hatter dropped his teacup and  
bread-and-butter, and went down on one knee.  
“ I ’m a poor man, your Majesty,” he began.  
“ You ’re a very poor speaker,” said the  
King.  
Here one of the guinea-pigs cheered, and was  
immediately suppressed by the officers of the  
court. (As that is rather a hard word, I will  
just explain to you how it was done. They had  
a large canvass bag, which tied up at the mouth  
with strings : into this they slipped the guinea-  
pig, head first, and then sat upon it.)  
“ I ’m glad I ’ve seen that done,” thought  
Alice. “ I ’ve so often read in the newspapers,  
at the end of trials, ‘ There was some attempt  
at applause, which was immediately suppressed  
by the officers of the court,’ and I never under-  
stood what it meant till now.”  
“ If that ’s all you know about it, you may  
stand down,” continued the King.  
“ I can’t go no lower,” said the Hatter : “ I ’m  

[1]: https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png
[2]: https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf
[3]: https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg
[4]: https://www.gutenberg.org/files/19778/19778-h/images/p001.png
